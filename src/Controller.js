
const api_url = 'https://citas-pol-marin.herokuapp.com/api/cita';

export default class Controller {

    static getCita = () => {
        return new Promise(
         (res, rej) => {
            fetch(api_url)
                .then(data => data.json())
                .then(datos => res(datos))
                .catch(err => {
                    rej(err);
                });
        });

    }
}