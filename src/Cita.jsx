import React, { useEffect, useState } from 'react';
import { Container, Row, Col } from 'reactstrap';
import Controller from './Controller'

const Cita = () => {

    const [cita, setCita] = useState({});

    useEffect(() => {
        Controller.getCita()
            .then(data => setCita(data))
            .catch(err => console.log(err.message));
    }, [])

    return (
        <>
            <Container className="m-5">
                <div className="cita-container">
                    <Row className="cita-card">
                        <Col xs="12" lg="7" className="imagen order-lg-1 order-2 p-0">
                            <img src={cita.img} alt={cita.cite} />
                        </Col>
                        <Col xs="12" lg="5" className="descrip order-lg-2 order-1 p-0">
                            <cite>{cita.cite}</cite>
                            <h4>{cita.person}</h4>
                        </Col>
                    </Row>
                </div>
            </Container>
        </>
    )
}

export default Cita;